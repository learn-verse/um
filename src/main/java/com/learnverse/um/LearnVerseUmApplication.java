package com.learnverse.um;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnVerseUmApplication {

  public static void main(String[] args) {
    SpringApplication.run(LearnVerseUmApplication.class, args);
  }

}
