package com.learnverse.um.config;

import javax.sql.DataSource;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class JooqConfig {

  @Autowired
  private DataSource dataSource;

  @Autowired
  private Environment env;

  @Bean
  public DSLContext dsl() {
    org.jooq.Configuration configuration = new DefaultConfiguration()
        .set(dataSource)
        .set(SQLDialect.valueOf(env.getProperty("spring.jooq.sql-dialect")));
    return DSL.using(configuration);
  }
}
