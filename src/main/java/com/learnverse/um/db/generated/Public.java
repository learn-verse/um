/*
 * This file is generated by jOOQ.
 */
package com.learnverse.um.db.generated;


import com.learnverse.um.db.generated.tables.ContentCreatorProfiles;
import com.learnverse.um.db.generated.tables.ContentCreators;
import com.learnverse.um.db.generated.tables.Databasechangelog;
import com.learnverse.um.db.generated.tables.Databasechangeloglock;
import com.learnverse.um.db.generated.tables.OrganisationUsers;
import com.learnverse.um.db.generated.tables.Organisations;
import com.learnverse.um.db.generated.tables.SuperAdmins;
import com.learnverse.um.db.generated.tables.UserProfiles;
import com.learnverse.um.db.generated.tables.Users;

import java.util.Arrays;
import java.util.List;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes", "this-escape" })
public class Public extends SchemaImpl {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>public</code>
     */
    public static final Public PUBLIC = new Public();

    /**
     * The table <code>public.content_creator_profiles</code>.
     */
    public final ContentCreatorProfiles CONTENT_CREATOR_PROFILES = ContentCreatorProfiles.CONTENT_CREATOR_PROFILES;

    /**
     * The table <code>public.content_creators</code>.
     */
    public final ContentCreators CONTENT_CREATORS = ContentCreators.CONTENT_CREATORS;

    /**
     * The table <code>public.databasechangelog</code>.
     */
    public final Databasechangelog DATABASECHANGELOG = Databasechangelog.DATABASECHANGELOG;

    /**
     * The table <code>public.databasechangeloglock</code>.
     */
    public final Databasechangeloglock DATABASECHANGELOGLOCK = Databasechangeloglock.DATABASECHANGELOGLOCK;

    /**
     * The table <code>public.organisation_users</code>.
     */
    public final OrganisationUsers ORGANISATION_USERS = OrganisationUsers.ORGANISATION_USERS;

    /**
     * The table <code>public.organisations</code>.
     */
    public final Organisations ORGANISATIONS = Organisations.ORGANISATIONS;

    /**
     * The table <code>public.super_admins</code>.
     */
    public final SuperAdmins SUPER_ADMINS = SuperAdmins.SUPER_ADMINS;

    /**
     * The table <code>public.user_profiles</code>.
     */
    public final UserProfiles USER_PROFILES = UserProfiles.USER_PROFILES;

    /**
     * The table <code>public.users</code>.
     */
    public final Users USERS = Users.USERS;

    /**
     * No further instances allowed
     */
    private Public() {
        super("public", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        return Arrays.asList(
            ContentCreatorProfiles.CONTENT_CREATOR_PROFILES,
            ContentCreators.CONTENT_CREATORS,
            Databasechangelog.DATABASECHANGELOG,
            Databasechangeloglock.DATABASECHANGELOGLOCK,
            OrganisationUsers.ORGANISATION_USERS,
            Organisations.ORGANISATIONS,
            SuperAdmins.SUPER_ADMINS,
            UserProfiles.USER_PROFILES,
            Users.USERS
        );
    }
}
