/*
 * This file is generated by jOOQ.
 */
package com.learnverse.um.db.generated.tables;


import com.learnverse.um.db.generated.Keys;
import com.learnverse.um.db.generated.Public;
import com.learnverse.um.db.generated.tables.records.ContentCreatorProfilesRecord;

import java.time.LocalDateTime;
import java.util.function.Function;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Function6;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Records;
import org.jooq.Row6;
import org.jooq.Schema;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes", "this-escape" })
public class ContentCreatorProfiles extends TableImpl<ContentCreatorProfilesRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>public.content_creator_profiles</code>
     */
    public static final ContentCreatorProfiles CONTENT_CREATOR_PROFILES = new ContentCreatorProfiles();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ContentCreatorProfilesRecord> getRecordType() {
        return ContentCreatorProfilesRecord.class;
    }

    /**
     * The column <code>public.content_creator_profiles.id</code>.
     */
    public final TableField<ContentCreatorProfilesRecord, Long> ID = createField(DSL.name("id"), SQLDataType.BIGINT.nullable(false).identity(true), this, "");

    /**
     * The column
     * <code>public.content_creator_profiles.content_creator_id</code>.
     */
    public final TableField<ContentCreatorProfilesRecord, Long> CONTENT_CREATOR_ID = createField(DSL.name("content_creator_id"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>public.content_creator_profiles.bio</code>.
     */
    public final TableField<ContentCreatorProfilesRecord, String> BIO = createField(DSL.name("bio"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.content_creator_profiles.expertise</code>.
     */
    public final TableField<ContentCreatorProfilesRecord, String> EXPERTISE = createField(DSL.name("expertise"), SQLDataType.VARCHAR(255), this, "");

    /**
     * The column <code>public.content_creator_profiles.created_at</code>.
     */
    public final TableField<ContentCreatorProfilesRecord, LocalDateTime> CREATED_AT = createField(DSL.name("created_at"), SQLDataType.LOCALDATETIME(6).nullable(false), this, "");

    /**
     * The column <code>public.content_creator_profiles.updated_at</code>.
     */
    public final TableField<ContentCreatorProfilesRecord, LocalDateTime> UPDATED_AT = createField(DSL.name("updated_at"), SQLDataType.LOCALDATETIME(6).nullable(false), this, "");

    private ContentCreatorProfiles(Name alias, Table<ContentCreatorProfilesRecord> aliased) {
        this(alias, aliased, null);
    }

    private ContentCreatorProfiles(Name alias, Table<ContentCreatorProfilesRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>public.content_creator_profiles</code> table
     * reference
     */
    public ContentCreatorProfiles(String alias) {
        this(DSL.name(alias), CONTENT_CREATOR_PROFILES);
    }

    /**
     * Create an aliased <code>public.content_creator_profiles</code> table
     * reference
     */
    public ContentCreatorProfiles(Name alias) {
        this(alias, CONTENT_CREATOR_PROFILES);
    }

    /**
     * Create a <code>public.content_creator_profiles</code> table reference
     */
    public ContentCreatorProfiles() {
        this(DSL.name("content_creator_profiles"), null);
    }

    public <O extends Record> ContentCreatorProfiles(Table<O> child, ForeignKey<O, ContentCreatorProfilesRecord> key) {
        super(child, key, CONTENT_CREATOR_PROFILES);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : Public.PUBLIC;
    }

    @Override
    public Identity<ContentCreatorProfilesRecord, Long> getIdentity() {
        return (Identity<ContentCreatorProfilesRecord, Long>) super.getIdentity();
    }

    @Override
    public UniqueKey<ContentCreatorProfilesRecord> getPrimaryKey() {
        return Keys.CONTENT_CREATOR_PROFILES_PKEY;
    }

    @Override
    public ContentCreatorProfiles as(String alias) {
        return new ContentCreatorProfiles(DSL.name(alias), this);
    }

    @Override
    public ContentCreatorProfiles as(Name alias) {
        return new ContentCreatorProfiles(alias, this);
    }

    @Override
    public ContentCreatorProfiles as(Table<?> alias) {
        return new ContentCreatorProfiles(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public ContentCreatorProfiles rename(String name) {
        return new ContentCreatorProfiles(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public ContentCreatorProfiles rename(Name name) {
        return new ContentCreatorProfiles(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public ContentCreatorProfiles rename(Table<?> name) {
        return new ContentCreatorProfiles(name.getQualifiedName(), null);
    }

    // -------------------------------------------------------------------------
    // Row6 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row6<Long, Long, String, String, LocalDateTime, LocalDateTime> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    public <U> SelectField<U> mapping(Function6<? super Long, ? super Long, ? super String, ? super String, ? super LocalDateTime, ? super LocalDateTime, ? extends U> from) {
        return convertFrom(Records.mapping(from));
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    public <U> SelectField<U> mapping(Class<U> toType, Function6<? super Long, ? super Long, ? super String, ? super String, ? super LocalDateTime, ? super LocalDateTime, ? extends U> from) {
        return convertFrom(toType, Records.mapping(from));
    }
}
