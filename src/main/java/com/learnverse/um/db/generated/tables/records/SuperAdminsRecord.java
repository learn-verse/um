/*
 * This file is generated by jOOQ.
 */
package com.learnverse.um.db.generated.tables.records;


import com.learnverse.um.db.generated.tables.SuperAdmins;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes", "this-escape" })
public class SuperAdminsRecord extends UpdatableRecordImpl<SuperAdminsRecord> implements Record2<Long, Long> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>public.super_admins.id</code>.
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.super_admins.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>public.super_admins.user_id</code>.
     */
    public void setUserId(Long value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.super_admins.user_id</code>.
     */
    public Long getUserId() {
        return (Long) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row2<Long, Long> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    @Override
    public Row2<Long, Long> valuesRow() {
        return (Row2) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return SuperAdmins.SUPER_ADMINS.ID;
    }

    @Override
    public Field<Long> field2() {
        return SuperAdmins.SUPER_ADMINS.USER_ID;
    }

    @Override
    public Long component1() {
        return getId();
    }

    @Override
    public Long component2() {
        return getUserId();
    }

    @Override
    public Long value1() {
        return getId();
    }

    @Override
    public Long value2() {
        return getUserId();
    }

    @Override
    public SuperAdminsRecord value1(Long value) {
        setId(value);
        return this;
    }

    @Override
    public SuperAdminsRecord value2(Long value) {
        setUserId(value);
        return this;
    }

    @Override
    public SuperAdminsRecord values(Long value1, Long value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached SuperAdminsRecord
     */
    public SuperAdminsRecord() {
        super(SuperAdmins.SUPER_ADMINS);
    }

    /**
     * Create a detached, initialised SuperAdminsRecord
     */
    public SuperAdminsRecord(Long id, Long userId) {
        super(SuperAdmins.SUPER_ADMINS);

        setId(id);
        setUserId(userId);
        resetChangedOnNotNull();
    }
}
